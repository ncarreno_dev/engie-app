import React, { useEffect, useState, useReducer } from 'react';
import { Root, Toast } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

import { AuthContext } from './context/auth-context';
import { handleSignIn, removeToken } from './api';
import Splash from './views/Splash';
import AuthLoading from './views/AuthLoading';

const authReducer = (state, action) => {
  switch (action.type) {
    case 'RESTORE_TOKEN':
      return {
        ...state,
        token: action.token,
        loading: false,
        isLoggedIn: action.token !== null,
      };
    case 'LOGIN':
      return {
        ...state,
        isLoggedIn: true,
        token: action.token,
        loading: false,
      };
    case 'LOGOUT':
      return {
        ...state,
        isLoggedIn: false,
        token: null,
        loading: false,
      };
    case 'LOADING':
      return {
        ...state,
        loading: action.loading,
      };
    default:
      return state;
  }
};

export default function App() {
  const [loading, setLoading] = useState('splash');
  const [state, dispatch] = useReducer(authReducer, {
    loading: true,
    isLoggedIn: false,
    token: null,
  });

  const authContext = {
    login: async (user, pass) => {
      handleSignIn(user, pass)
        .then((res) => {
          dispatch({ type: 'LOGIN', token: res });
        })
        .catch((err) => {
          Toast.show({
            text: `${err}`,
            duration: 3000,
            position: 'bottom',
            type: 'danger',
          });
        });
    },
    logout: async () => {
      try {
        await removeToken();
        dispatch({ type: 'LOGOUT' });
        Toast.show({
          text: 'Sesión terminada',
          duration: 3000,
          position: 'bottom',
          type: 'success',
        });
      } catch (err) {
        dispatch({ type: 'LOADING', loading: false });
        Toast.show({
          text: 'Error al terminar sesión',
          duration: 3000,
          position: 'bottom',
          type: 'danger',
        });
      }
    },
    restoreToken: async () => {
      let userToken = null;
      try {
        userToken = await AsyncStorage.getItem('token');
      } catch (err) {}
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    },
    isLoggedIn: state.isLoggedIn,
    loading: state.loading,
  };

  useEffect(() => {
    setTimeout(() => {
      setLoading('');
    }, 3000);
  }, []);

  if (loading === 'splash') {
    return <Splash />;
  }
  return (
    <AuthContext.Provider value={authContext}>
      <Root>
        <AuthLoading />
      </Root>
    </AuthContext.Provider>
  );
}
