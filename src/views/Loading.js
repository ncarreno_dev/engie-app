import React from 'react';
import { View } from 'react-native';
import Container from '../components/Container';
import Spinner from '../components/Spinner';

export default function Loading() {
  return (
    <Container bgColor="#0F1E29">
      <Spinner />
    </Container>
  );
}
