import React, { useState, useContext, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import Container from '../components/Container';
import Input from '../components/Input';
import Header from '../components/Header';
import Button from '../components/Button';

import { AuthContext } from '../context/auth-context';
import { Toast } from 'native-base';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    margin: 10,
    alignSelf: 'stretch',
    marginLeft: 20,
    padding: 10,
    width: '100%',
    height: 100,
  },
  titleTxt: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: 'Lato-Bold',
  },
});

const validator = (value, password = false) => {
  const regExp = /^[a-zA-Z0-9_]*$/;
  if (password) {
    const passRegExp = /^[a-zA-Z0-9_!/@*¡?¿$ªº]*$/;
    return passRegExp.test(value);
  }
  return regExp.test(value);
};

const Login = (props) => {
  const auth = useContext(AuthContext);

  const [userId, setUserId] = useState('');
  const [password, setPassword] = useState('');
  const [hidden, setHidden] = useState(true);
  const OnChangeUserId = (value) => {
    if (validator(value)) {
      setUserId(value);
    } else {
      Toast.show({
        text: 'Carácter no permitido',
        duration: 3000,
        type: 'danger',
        position: 'bottom',
      });
    }
  };

  const OnChangePassword = (value) => {
    if (validator(value, true)) {
      setPassword(value);
    } else {
      Toast.show({
        text: 'Carácter no permitido',
        duration: 3000,
        type: 'danger',
        position: 'bottom',
      });
    }
  };

  const handleLogin = () => {
    if (userId.length && password.length) {
      auth.login(userId, password);
    } else if (!userId.length && password.length) {
      Toast.show({
        text: 'Debe ingresar el id de usuario',
        duration: 3000,
        type: 'danger',
        position: 'bottom',
      });
    } else if (userId.length && !password.length) {
      Toast.show({
        text: 'Debe ingresar una contraseña',
        duration: 3000,
        type: 'danger',
        position: 'bottom',
      });
    }
  };

  return (
    <>
      <Header />
      <Container bgColor="#1E2D37" justifyContent="center">
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={styles.container}>
          <View style={styles.title}>
            <Text style={styles.titleTxt}>
              Inicio Sesión
              {'\n'}
              Monitoreo Operacional
            </Text>
          </View>
          <Input
            value={userId}
            onChange={OnChangeUserId}
            placeholder="Usuario ID"
          />
          <Input
            value={password}
            placeholder="Contraseña Windows"
            hidden={hidden}
            onChange={OnChangePassword}
            iconColor="#bdbdbd"
            iconName="eye"
            sizeIcon={20}
            onPressIcon={() => setHidden((prevHidden) => !prevHidden)}
          />
          <Button value="Ingresar" onPress={handleLogin} />
        </KeyboardAvoidingView>
      </Container>
    </>
  );
};

export default Login;
