import React, { useEffect, useContext } from 'react';
// import AsyncStorage from '@react-native-community/async-storage';
// import { Toast } from 'native-base';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AuthStack from '../routes/auth';
import HomeStack from '../routes/app';
import Spinner from '../components/Spinner';
import { AuthContext } from '../context/auth-context';

const Stack = createStackNavigator();

const AuthLoading = (props) => {
  const auth = useContext(AuthContext);

  useEffect(() => {
    auth.restoreToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (auth.loading) {
    return <Spinner />;
  }
  const routes = auth.isLoggedIn ? (
    <Stack.Screen name="home" component={HomeStack} />
  ) : (
    <Stack.Screen name="login" component={AuthStack} />
  );

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">{routes}</Stack.Navigator>
    </NavigationContainer>
  );
};

export default AuthLoading;
