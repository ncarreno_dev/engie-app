import React, { useContext } from 'react';
import { View } from 'react-native';
import { AuthContext } from '../context/auth-context';

export default function Logout() {
  const auth = useContext(AuthContext);
  auth.logout();
  return null;
}
