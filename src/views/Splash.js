import React from 'react';
import { Image, Dimensions, StyleSheet } from 'react-native';
import Container from '../components/Container';

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  logo: {
    width: width * 0.5,
    height: height * 0.2,
  },
});
const Splash = props => {
  return (
    <Container bgColor="#0F1E29">
      <Image
        style={styles.logo}
        resizeMode="stretch"
        source={{
          uri:
            'https://www.revistaei.cl/wp-content/uploads/2016/06/Logo-ENGIE-azul-1024x614.png',
        }}
      />
    </Container>
  );
};

export default Splash;
