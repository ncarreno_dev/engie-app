import React from 'react';
import { Text } from 'react-native';
import Container from '../components/Container';

export default function State() {
  return (
    <Container>
      <Text style={{ fontFamily: 'Lato-Bold', fontSize: 16, color: '#bdbdbd' }}>
        ESTADOS
      </Text>
    </Container>
  );
}
