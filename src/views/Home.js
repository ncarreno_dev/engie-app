/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Container from '../components/Container';
import Button from '../components/Button';
import ProgressBar from '../components/ProgressBar';
import { Content, Accordion, Card, CardItem, Body } from 'native-base';
import { CardBody, CardItem as CardRow } from '../components/Card';
import Icon from 'react-native-vector-icons/FontAwesome5';

const styles = StyleSheet.create({
  accordion: {
    marginTop: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#181f24',
  },
  body: {
    width: '100%',
    height: '100%',
  },
  header: {
    flexDirection: 'row',
    width: '100%',
    padding: 20,
    height: 60,
    justifyContent: 'space-between',
    backgroundColor: '#2B485C',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  headerLeft: {
    width: '40%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  headerRight: {
    width: '30%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row-reverse',
  },
  card: {
    backgroundColor: '#1E2D37',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#181f24',
  },
});

const WaterCard = () => {
  return (
    <CardBody>
      <CardRow
        bodyLeft={<BodyTitle />}
        bodyRight={<RightWaterCard />}
        divider
      />
      <CardRow
        bodyLeft={<BodyTitle title="CONSUMO" value="1234 m3/h" />}
        bodyRight={<RightWaterCard />}
      />
    </CardBody>
  );
};

const RightWaterCard = () => (
  <Button
    value="ver gráfico"
    borderColor="#009de9"
    borderRadius={50}
    txtColor="#009de9"
    borderWidth={1}
    btnWidth={100}
    btnHeight={30}
    paddingVertical={10}
    bgColor="#1E2D37"
  />
);

const HeaderLeft = ({ title }) => {
  return (
    <>
      <Icon name="tint" color="white" size={20} />
      <Text
        style={{
          color: '#bdbdbd',
          fontFamily: 'Lato-Bold',
          fontSize: 18,
          marginLeft: 15,
        }}>
        {title}
      </Text>
    </>
  );
};
const HeaderRight = (props) => {
  return (
    <View style={{ flexDirection: 'row' }}>
      <Text
        style={{
          color: '#bdbdbd',
          fontFamily: 'Lato-Bold',
          fontSize: 18,
          marginRight: 15,
        }}>
        Desmin
      </Text>
      <Icon
        name={props.iconName}
        size={20}
        color="#009de9"
        onPress={props.onPress}
      />
    </View>
  );
};

const BodyTitle = ({ title = 'STOCK', value = '1234 m3' }) => {
  return (
    <>
      <View
        style={{
          width: '100%',
          justifyContent: 'flex-start',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Icon name="circle" solid color="#6ed2b1" />
        <Text
          style={{
            color: '#bdbdbd',
            fontFamily: 'Lato-Bold',
            fontSize: 16,
            marginLeft: 10,
          }}>
          {title}
        </Text>
      </View>
      <Text
        style={{
          color: '#bdbdbd',
          fontFamily: 'Lato-Bold',
          fontSize: 20,
          marginLeft: 20,
          marginTop: 10,
        }}>
        {' '}
        {value}
      </Text>
    </>
  );
};

const dataArray = [
  {
    title: 'Agua Total',
    content: [{ name: 'STOCK', value: '1234m3', text: 'ver gráfico' }],
  },
];
export default function Home() {
  const customHeader = (item, expanded) => {
    return (
      <View style={styles.header}>
        <View style={styles.headerLeft}>
          <HeaderLeft title={item.title} />
        </View>
        <View style={styles.headerRight}>
          <HeaderRight iconName={expanded ? 'chevron-up' : 'chevron-down'} />
        </View>
      </View>
    );
  };
  const customContent = () => {
    return <WaterCard />;
  };
  return (
    <Container
      bgColor="#181f24"
      justifyContent="flex-start"
      paddingHorizontal={20}>
      <Content padder>
        <Card style={styles.card}>
          <CardItem
            style={{
              backgroundColor: '#1E2D37',
            }}>
            <Body
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  color: '#bdbdbd',
                  fontSize: 16,
                  fontFamily: 'Lato-Bold',
                }}>
                {' '}
                Potencia bruta total{' '}
              </Text>
              <Text
                style={{
                  color: '#6ed2b1',
                  fontSize: 16,
                  fontFamily: 'Lato-Bold',
                }}>
                {' '}
                641 MW{' '}
              </Text>
            </Body>
          </CardItem>
          <CardItem style={{ backgroundColor: '#1E2D37' }}>
            <ProgressBar />
          </CardItem>
          <Text
            style={{
              alignSelf: 'flex-start',
              fontStyle: 'Lato-Light',
              color: '#95a3ad',
              paddingLeft: 10,
              marginBottom: 10,
            }}>
            * Considera la suma de las 6 unidades
          </Text>
          <View
            style={{
              height: 0.5,
              backgroundColor: '#eeeeee',
              width: '100%',
              marginVertical: 10,
            }}
          />
          <CardItem footer style={{ backgroundColor: '#1E2D37' }}>
            <Accordion
              style={{
                backgroundColor: '#1E2D37',
              }}
              dataArray={[{ title: 'ver detalle por unidad' }]}
              renderHeader={(item) => (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      color: '#bdbdbd',
                      fontSize: 16,
                      fontFamily: 'Roboto-Medium',
                    }}>
                    {item.title}
                  </Text>
                  <Icon name="chevron-down" color="#009de9" size={20} />
                </View>
              )}
              renderContent={() => <View />}
            />
          </CardItem>
        </Card>
        <Accordion
          style={styles.accordion}
          dataArray={dataArray}
          renderHeader={(item, expanded) => customHeader(item, expanded)}
          renderContent={(item) => customContent(item)}
        />
      </Content>
    </Container>
  );
}
