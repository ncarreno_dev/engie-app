import axios from '../services/axios';
import AsyncStorage from '@react-native-community/async-storage';

export const handleSignIn = async (userName, password) => {
  try {
    const res = await axios.post('/api/login/Logon', {
      Username: userName,
      Password: password,
    });
    const { Token } = await res.data;
    await AsyncStorage.setItem('token', Token);
    return Token;
  } catch (err) {
    throw new Error('Error en autenticación');
  }
};

export const removeToken = async () => {
  await AsyncStorage.removeItem('token');
};
