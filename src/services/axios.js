import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const baseURL = 'https://pme.engie.cl';

const axiosInstance = axios.create({
  baseURL,
});

axiosInstance.interceptors.request.use(async config => {
  const token = await AsyncStorage.getItem('token');

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default axiosInstance;
