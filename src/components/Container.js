import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: width,
    // height: height,
    flex: 1,
    backgroundColor: '#1e2d37',
  },
});

export default function Container({
  bgColor,
  children,
  justifyContent,
  paddingHorizontal,
}) {
  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: bgColor,
          justifyContent: justifyContent,
          paddingHorizontal: paddingHorizontal,
        },
      ]}
    >
      {children}
    </View>
  );
}

Container.defaultProps = {
  bgColor: '#1e2d37',
  children: {},
  justifyContent: 'center',
};
