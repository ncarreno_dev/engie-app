import React from 'react';
import { StyleSheet, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    flex: 1,
    maxHeight: '50%',
    width: '100%',
    marginVertical: 20,
    borderColor: '#181f24',
    backgroundColor: '#1E2D37',
  },
  bodyRow: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    marginVertical: 20,
  },
  bodyLeft: {
    flexDirection: 'column',
    width: '50%',
    justifyContent: 'center',
  },
  bodyRight: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 30,
    marginTop: 10,
    flexDirection: 'row',
  },
  divider: {
    padding: 0,
    margin: 0,
    width: '100%',
    height: 0.5,
    backgroundColor: '#bdbdbd',
  },
  header: {
    flexDirection: 'row',
    width: '100%',
    padding: 20,
    height: 60,
    justifyContent: 'space-between',
  },
  body: {
    width: '100%',
    height: '100%',
  },
  headerLeft: {
    width: '40%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  headerRight: {
    width: '30%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row-reverse',
  },
  cardBody: {
    backgroundColor: '#1E2D37',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderBottomColor: '#181f24',
  },
});

export function Card({ bgColor, headerLeft, headerRight, cardBody }) {
  return (
    <View style={styles.container}>
      <View
        style={[
          styles.header,
          // eslint-disable-next-line react-native/no-inline-styles
          {
            backgroundColor: bgColor,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
          },
        ]}>
        <View style={styles.headerLeft}>{headerLeft}</View>
        <View style={styles.headerRight}>{headerRight}</View>
      </View>
      <View style={styles.body}>{cardBody}</View>
    </View>
  );
}

export const CardBody = ({ children }) => {
  return <View style={styles.cardBody}>{children}</View>;
};

export const CardItem = ({ bodyLeft, bodyRight, divider = false }) => {
  return (
    <>
      <View style={styles.bodyRow}>
        <View style={styles.bodyLeft}>{bodyLeft}</View>
        <View style={styles.bodyRight}>{bodyRight}</View>
      </View>
      {divider && <View style={styles.divider} />}
    </>
  );
};
