import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';

const colors = ['#181f24', '#0F1E29', '#1E2D37', '#33414A', '#2B485C'];

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  row: {
    width,
    padding: 20,
    backgroundColor: '#0F1E29',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  divider: {
    height: 1,
    opacity: 0.5,
    padding: 3,
    backgroundColor: '#020202',
    width,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowRadius: 1,
    elevation: 10,
    shadowColor: '#000',
    shadowOpacity: 0.4,
  },
});

export default function componentName({ children }) {
  return (
    <>
      <View style={styles.row}>{children}</View>
      <View style={styles.divider} />
    </>
  );
}
