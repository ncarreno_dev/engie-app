import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
  root: {
    width: width,
    alignItems: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#181f24',
    shadowOffset: {
      width: 4,
      height: 7,
    },
    shadowOpacity: 0.3,
    shadowRadius: 10,
    elevation: 8,
  },
  btnText: {
    textAlign: 'center',
    fontFamily: 'Lato-Bold',
  },
});

export default function Button({
  value,
  onPress,
  btnWidth,
  btnHeight,
  borderRadius,
  borderWidth,
  borderColor,
  bgColor,
  fontSize,
  paddingVertical,
  txtColor,
}) {
  return (
    <View style={styles.root}>
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.button,
          {
            width: btnWidth,
            height: btnHeight,
            borderRadius: borderRadius,
            borderWidth: borderWidth,
            borderColor: borderColor,
            backgroundColor: bgColor,
            paddingVertical: paddingVertical,
          },
        ]}
      >
        <Text style={[styles.btnText, { fontSize: fontSize, color: txtColor }]}>
          {value}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

Button.defaultProps = {
  btnWidth: '90%',
  borderRadius: 10,
  borderWidth: 1,
  borderColor: '#1e2d37',
  bgColor: '#009de9',
  btnHeight: 70,
  fontSize: 16,
  paddingVertical: 20,
  txtColor: '#ffffff',
};
