import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  container: {
    margin: 5,
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
    height: 5,
    width: Dimensions.get('window').width * 0.75,
    backgroundColor: '#33414a',
  },
});

const ProgressBar = ({ width = 80, bgColor = '#6ed2b1' }) => {
  const theme = {
    width: width,
    flex: 1,
    backgroundColor: bgColor,
  };
  return (
    <View style={styles.container}>
      <View style={theme} />
    </View>
  );
};

export default ProgressBar;
