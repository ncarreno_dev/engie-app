import React from 'react';
import { Dimensions, StyleSheet, TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  root: {
    width: width * 0.9,
    marginBottom: 20,
    alignItems: 'center',
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#1e2d37',
    backgroundColor: '#2B485C',
  },
  input: {
    paddingLeft: 10,
    flex: 1,
    color: '#bdbdbd',
    paddingVertical: 10,
  },
});

const Input = ({
  placeholder,
  value,
  hidden,
  onChange,
  onPressIcon,
  iconName,
  sizeIcon,
  iconColor,
}) => {
  return (
    <View style={styles.root}>
      <TextInput
        secureTextEntry={hidden}
        style={styles.input}
        placeholderTextColor="#bdbdbd"
        value={value}
        placeholder={placeholder}
        onChangeText={onChange}
      />
      <Icon
        onPress={onPressIcon}
        name={iconName}
        size={sizeIcon}
        color={iconColor}
      />
    </View>
  );
};

export default Input;
