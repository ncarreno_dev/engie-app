import React from 'react';
import { ActivityIndicator, Text } from 'react-native';
import Container from './Container';

export default function Spinner() {
  return (
    <Container>
      <ActivityIndicator size={80} color="#009de9" />
      <Text style={{ color: 'white', marginTop: 20 }}> Cargando... </Text>
    </Container>
  );
}
