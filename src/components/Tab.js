import React, { useRef, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
  btnTab: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
    fontSize: 16,
    color: '#BDBDBD',
  },
  underline: {
    bottom: 0,
    position: 'absolute',
    height: 1,
    backgroundColor: '#009de9',
    width: '90%',
  },
  tabs: {
    flexDirection: 'column',
    flex: 1,
  },
});

export default function Tab(props) {
  const [focus, setFocus] = useState(false);
  const handleOnPress = () => {
    setFocus(prevFocus => !prevFocus);
  };
  return (
    <View style={styles.tabs}>
      <TouchableOpacity style={styles.btnTab} onPress={handleOnPress}>
        <Text style={styles.title}>{props.title}</Text>
        {focus && <View style={styles.underline} />}
      </TouchableOpacity>
    </View>
  );
}
