import React from 'react';
import { Dimensions, Image, StyleSheet, View } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  header: {
    width: width,
    height: height * 0.2,
    justifyContent: 'center',
    paddingLeft: 10,
    backgroundColor: '#1E2D37',
  },
  logo: {
    width: 68,
    height: 24,
    padding: 10,
    marginLeft: 20,
  },
  divider: {
    height: 1,
    width: width,
    backgroundColor: '#eee',
  },
});
const Header = () => {
  return (
    <>
      <View style={styles.header}>
        <Image
          style={styles.logo}
          resizeMode="contain"
          source={require('../../assets/images/logo-engie-white.png')}
        />
      </View>
      <View style={styles.divider} />
    </>
  );
};

export default Header;
