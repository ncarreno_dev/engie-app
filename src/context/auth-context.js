import { createContext } from 'react';

export const AuthContext = createContext({
  isLoggedIn: false,
  loading: true,
  login: () => {},
  restoreToken: () => {},
  logout: () => {},
});
