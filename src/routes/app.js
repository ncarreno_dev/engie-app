import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { View } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';

import Home from '../views/Home';
import States from '../views/State';
import Logout from '../views/Logout';

const Stack = createStackNavigator();

const BottomTab = createBottomTabNavigator();
const TopTab = createMaterialTopTabNavigator();

const Drawer = createDrawerNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="home"
        component={HomeTopTab}
        options={({ navigation }) => ({
          headerTitle: 'Monitoreo Operacional',
          headerTitleStyle: {
            fontFamily: 'Roboto-Medium',
            color: '#bdbdbd',
          },
          headerStyle: {
            backgroundColor: '#0F1E29',
          },
          headerLeft: () => {
            return (
              <Icon
                name="bars"
                size={20}
                color="#bdbdbd"
                style={{ margin: 10 }}
                onPress={() => navigation.toggleDrawer()}
              />
            );
          },
          headerRight: () => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  flex: 0.4,
                  justifyContent: 'space-between',
                  marginHorizontal: 10,
                }}
              >
                <Icon name="sync-alt" size={20} color="#bdbdbd" />
                <View style={{ width: 10 }} />
                <Icon name="bell" size={20} color="#bdbdbd" />
              </View>
            );
          },
        })}
      />
    </Stack.Navigator>
  );
};

const HomeDrawerNavigator = () => {
  return (
    <Drawer.Navigator
      drawerType="slide"
      drawerStyle={{
        backgroundColor: '#0F1E29',
        paddingTop: 40,
        justifyContent: 'space-between',
      }}
      drawerContentOptions={{
        activeTintColor: '#009de9',
        inactiveTintColor: '#bdbdbd',
      }}
    >
      <Drawer.Screen
        name="home"
        component={BottomTabNavigator}
        options={({ navigation }) => ({
          title: 'Inicio',
          drawerIcon: ({ color, size }) => (
            <Icon name="home" color={color} size={size} />
          ),
        })}
      />
      <Drawer.Screen
        name="logout"
        component={Logout}
        options={({ navigation }) => ({
          title: 'Cerrar sesión',
          drawerIcon: ({ color, size }) => (
            <Icon name="sign-out-alt" size={size} color={color} />
          ),
        })}
      />
    </Drawer.Navigator>
  );
};

const HomeTopTab = () => {
  return (
    <TopTab.Navigator
      tabBarOptions={{
        activeTintColor: '#eee',
        inactiveTintColor: '#bdbdbd',
        indicatorStyle: {
          backgroundColor: '#009de9',
          width: '40%',
          marginHorizontal: 15,
        },
        indicatorContainerStyle: {
          backgroundColor: '#0F1E29',
        },
      }}
    >
      <TopTab.Screen name="CTM" component={Home} />
      <TopTab.Screen name="CTT" component={Home} />
    </TopTab.Navigator>
  );
};

const BottomTabNavigator = () => {
  return (
    <BottomTab.Navigator
      tabBarOptions={{
        inactiveBackgroundColor: '#181f24',
        activeBackgroundColor: '#1E2D37',
        activeTintColor: '#eeeeee',
        inactiveTintColor: '#bdbdbd',
        style: {
          borderColor: 'black',
          backgroundColor: 'black',
        },
        tabStyle: {
          borderTopColor: 'black',
          borderTopWidth: 1,
        },
      }}
    >
      <BottomTab.Screen
        component={HomeStack}
        name="inicio"
        options={() => ({
          tabBarIcon: ({ color, size }) => (
            <Icon name="home" color={color} size={size} />
          ),
        })}
      />
      <BottomTab.Screen
        component={States}
        name="estados"
        options={() => ({
          tabBarIcon: ({ color, size }) => (
            <Icon name="tachometer-alt" color={color} size={size} />
          ),
        })}
      />
    </BottomTab.Navigator>
  );
};

export default HomeDrawerNavigator;
